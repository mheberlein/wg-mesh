# Wireguard VPN Mesh Sandbox

## Mesh Notes

- every peer needs to have all other peers with public keys in their configs to accept their packets
- the VPN adresses are useless without publicly accessible endpoints
- thanks to `PersistentKeepAlive`, all home peers keep sending packets to the hub
- this way the hub knows their endpoints and they remain open
- we'll manually propagate peer endpoints to the home configs after the first handshake
- with updated configs that contain peer endpoints, all peers can talk to each other
- if a home peer loses and reopens a connection to the hub, the endpoint may have changed and must be updated in the other configs again

## Run Hub

- clone this repository to a cloud VM with UDP port 51820 open:
  ```bash
  git clone https://gitlab.com/mheberlein/wireguard-sandbox.git
  ```
- run the hub instance and watch the interface status
  ```bash
  docker-compose run hub
  wg-quick up wg0
  watch wg
  ```

## Run Home1/2

- clone this repository to a local machine:
  ```bash
  git clone https://gitlab.com/mheberlein/wireguard-sandbox.git
  ```
- in both `home*/wg0.conf` files, replace "hub" with the cloud VM's public IP
  ```ini
    [Peer]  # hub
    AllowedIPs = 10.0.0.1/32
    PublicKey = VDqkHFeyTE1KHXCZhFVQGhVT6yJkHpR6nWDfQXCEjgY=
  + Endpoint = x.x.x.x:51820
    PersistentKeepAlive = 10
  ```
- start both home instances in separate terminals:
  ```bash
  docker-compose run home1
  wg-quick up wg0
  ping 10.0.0.1
  ping 10.0.0.3  # not working because endpoint missing
  ```
  ```bash
  docker-compose run home2
  wg-quick up wg0
  ping 10.0.0.1
  ping 10.0.0.2  # not working because endpoint missing
  ```

## Update Endpoints

- from the cloud `hub` status, copy the peer `gUHb...`'s endpoint into `home1/wg0.conf`:
  ```ini
    [Peer]  # home2
    AllowedIPs = 10.0.0.3/32
    PublicKey = gUHbH0jn4MpGd4z7ZQgBRasY8PTytOvOt9u2jBJUMEU=
  + Endpoint = x.x.x.x:12345
  ```
- in the `home1` instance, update the interface:
  ```
  wg syncconf <(wg-quick strip wg0)
  ```
- now ping from `home1` to `home2` again:
  ```bash
  ping 10.0.0.3  # expected to work
  ```
- because this tells `home2` about `home1`'s endpoint as well, we should be able to ping back from `home2` to `home1`:
  ```bash
  ping 10.0.0.2  # expected to work
  ```
